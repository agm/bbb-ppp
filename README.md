# Code for Integrating Cellular Module with BeagleBone Black (BBB)

This repo contains code for integrating SIMCom modules with BeagleBone Black using ppp/pppd. 

### Install PPP

```
sudo apt-get update
sudo apt-get install ppp screen elinks
```

### Download the PPP config file 

Download and copy `cellConfig` file into `/etc/ppp/peers/`

### Modifying the configuration file 

#### Selecting Serial Port

You can connect the cellular module to BBB through UART or USB. This can be done by changing the following line of code 

```
/dev/ttyO4
```

The placeholder is UART but it can be changed to USB.

#### Changing APN Value 

Modifying the following line of code to change the APN Value. In the code below it is `internet`

```
connect "/usr/sbin/chat -v -f /etc/chatscripts/gprs -T internet"
```

### Start PPP

```
sudo pon cellConfig
```

### Check Status and Debug

```
cat /var/log/syslog | grep pppd
```

You should be able to see an assigned IP address. This can also be verified with `ifconfig`. You should see `ppp0`.


Check `PPPD` log for errors

```
cat /var/log/syslog | grep chat
```

### Close Connection

```
sudo poff cellConfig
```

### Connection on Boot 

Add the `ppp0` interface to the `/etc/network/interfaces`

```
auto cellConfig
iface cellConfig inet ppp
	provider cellConfig
```



Adapted from https://www.adafruit.com
